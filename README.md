
## 项目简介
BaseCloud是一套基于uniapp、uniCloud、uni-id的全栈开发框架，不依赖任何第三方框架，极度精简轻巧。

本模块为BaseCloud用户端APP版本更新业务模块，包含用户端、管理端界面和接口，界面的色彩将与您项目的主题色一致。

本模块依赖BaseCloud全栈开发框架，如果您对BaseCloud不了解，请先 [阅读BaseCloud框架使用说明文档](https://ext.dcloud.net.cn/plugin?id=2481)。

## 项目预览

用户客户端APP版本更新登录业务模块：

用户端演示：

[查看H5演示APP更新效果 << ](http://base-cloud.joiny.cn/h5/#/pages/userCenter/userCenter)

## 项目预览图

![](https://vkceyugu.cdn.bspapp.com/VKCEYUGU-base-cloud/20c85f40-ddd0-11ea-b244-a9f5e5565f30.gif)

管理端管理功能：

![](https://vkceyugu.cdn.bspapp.com/VKCEYUGU-base-cloud/10a3f870-ddd7-11ea-8a36-ebb87efcf8c0.png)
![](https://vkceyugu.cdn.bspapp.com/VKCEYUGU-base-cloud/9ab75f30-ddd6-11ea-b997-9918a5dda011.png)

## 如何安装APP版本更新业务模块

1. 请先下载BaseCloud-updateVersion业务模块zip包，请勿直接导入Hbuilder中
2. zip包仅包含APP更新业务模块需要的管理端文件和用户端文件，不包含BaseCloud框架基础文件。

>如果您还没有下载BaseCloud框架基础文件，请先下载后使用本项目包。
>如果您的BaseCloud还未升级至最新版本，请先升级至最新版本后，集成此模块。

3. 集成指南：
zip包中的`adminClient`文件夹为管理端文件，包含服务端接口和管理端前端页面。
zip包中的`userClient`文件夹为用户端文件，包含用户端接口、和版本更新组件，以及一个组件使用示例页面（可删除）。

4. 请按目录结构依次复制到您自己的BaseCloud项目中去即可完成业务模块的集成。
5. 另外请注意：adminClient包含了数据初始化文件db_init.json，为管理后台管理功能的菜单配置数据。您也可以自行配置菜单数据：版本列表、版本编辑、版本删除。

6. 版本更新检查接口不需要任何权限，拦截器清除配置，请打开 `cloudfunctions > common > base-cloud > config.js ` 配置拦截规则，对版本检查接口放行：

```js
loginInter: {
	handle : [] , //拦截的路径，此处留空表示拦截全部的路径
	clear : [ //配置要清除拦截器的路径，注意：如果配置了handle则此处的配置无效。
		"admin/user/login", 
		"admin/user/checkToken",
		"api/version" //对版本检查接口放行
	] , 
	invoke:async function(attrs){
		//.....
	}
}
```

7. 上传云函数公共模块和 `api` 、 `admin` 函数 。

## 版本更新组件使用说明

|属性	|类型	|说明	|
|--	|--	|--	|
|title	|String	|标题，默认为：发现新版本	|
|defaultContent	|String	|如果接口未返回，默认显示的版本更新内容，默认为：快来升级，体验最新的功能吧！	|
|showVersion	|Boolean	|组件是否展示版本号信息，默认为true，一般用于点击版本号更新的场景；如果不展示版本号信息，则组件不可见，一般用于自动更新的场景	|
|autoShow	|Boolean	|检查到有新版本，是否立即显示更新弹窗，默认为false，在版本号旁边显示红点；|
|isCache	|Boolean	|版本检测的数据是否启用缓存策略，短时间内不重复发送服务端请求，默认为true，适用于应用首页等频繁访问刷新渲染组件的场景|

## 如何使用

在管理端，我们可以对版本进行管理，支持多个APP，支持区分安卓和IOS平台；
在用户APP端，我们通过插入组件的方式，来使用版本更新检查和更新功能，如下为版本更新使用示例：

1. 展示版本号，有新版本显示小红点，点击版本号后更新应用：

```html
//html代码
<version-update ref="version" @tap="toUpdate" :showVersion="true" :autoShow="false"></version-update>
```
```js
//js代码
methods:{
	toUpdate:function(e){
		this.$refs.version.toUpdate(); //调用组件的显示更新弹窗的方法。
	}
}
```

2. 不展示版本号，自动更新：

```html
//html代码
<version-update :showVersion="false" :autoShow="true"></version-update>
```

## 使用过程中如有问题，请加BaseCloud用户交流QQ群：

群号：649807152

[点击链接，直接加入qq群 << ](https://qm.qq.com/cgi-bin/qm/qr?k=upb9fG80Wpsls_At8ZI01QTqDu_0KyUL&jump_from=webapi)

![](https://vkceyugu.cdn.bspapp.com/VKCEYUGU-base-cloud/e699d5d0-ddd7-11ea-9dfb-6da8e309e0d8.png)