const authInter = require("./intercepters/authInter") ;

module.exports = {
	isDebug : true , //会输出一些日志到控制台，方便调试
	inters:{ //配置全局拦截器
		loginInter: {
			handle : [] , //拦截的路径，此处留空表示拦截全部的路径
			clear : [ //配置要清除拦截器的路径，注意：如果配置了handle则此处的配置无效。
				"admin/user/login", 
				"admin/user/checkToken",
				/^api\/userLogin/,
				"api/version",
			] , 
			invoke:async function(attrs){//拦截器函数，入参为上一个拦截器通过setAttr方法传递的所有的键值对
				const {event , ctx , uniID } = this ;
				var res = await uniID.checkToken(event.uniIdToken);
				if(res.code){
					return {
						state : 'needLogin',
						msg : "请登录"
					};
				}
				//将user传入下一个拦截器，在拦截器函数的参数中可以获取到，也可以通过this.getAttr("user")来取到该值。
				this.setAttr({user : res.userInfo}); 
				//当前拦截器放行
				this.next();
			}
		},
		authInter  ,
	}
}