/**
 * 用户端的API接口
 */
'use strict';
const BaseCloud = require("base-cloud");

exports.main = async ( event , ctx ) => {
	return await new BaseCloud({ event , ctx , fnName : "api" }).invoke(`${__dirname}/controller`);
};