'use strict';
const db = uniCloud.database();
const dbCmd = db.command ;
const $ = dbCmd.aggregate ;
const Version = db.collection("t_version");

module.exports = async function(e){
	var { versionName } = this.params ;
	var platform = this.ctx.PLATFORM ;
	var appId = this.ctx.APPID ;
	var data = this.findFirst( await Version.where({ platform , appId }).orderBy("updateTime" , "desc").get() );
	if ( null == data || data.versionName == versionName ) {
		return { updated : false } ;
	}
	var {downloadUrl,remark,version} = data ;
	return { downloadUrl,remark,version , updated : true } ;
};