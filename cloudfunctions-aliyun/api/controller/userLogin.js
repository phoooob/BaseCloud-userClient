'use strict';
const db = uniCloud.database();
const dbCmd = db.command ;
const $ = dbCmd.aggregate ;
const User = db.collection("uni-id-users");

module.exports = {
	
	sendCode:async function(e){
		var {mobile , type} = this.params ; //type:login登录、bind绑定手机、unbind解绑手机
		var code = Math.ceil( Math.random()*1000000) + "" ;
		var sendRes = await this.uniID.sendSmsCode({mobile:mobile+"",code,type});
		return !!sendRes.code? this.fail(sendRes.msg) : this.ok("发送成功") ;
	},
	
	loginByMobileCode : async function(e){
		var {mobile , code} = this.params ;
		var res = await this.uniID.loginBySms({mobile : mobile+"" , code : code+""});
		return !!res.code ? {...this.fail(res.msg) , mobile , code  } : { ...this.ok("登录成功") , token : res.token } ;
	},
	
	checkToken: async function(e){
		var payload = await this.uniID.checkToken(this.event.uniIdToken);
		return !payload.code ? this.ok() : this.fail();
	}
};